### 系统菜单列表
#sql("resource.list")
	select * from t_sys_resource where 1 = 1
	#if(keyword)
		and name like #like(keyword)
	#end
	#if(state)
		and state = #para(state)
	#end
	#if(null!=id)
		and parent_id = #para(id)
	#end	
	#if(null!=userId)
		and exists (select 1 from t_sys_role_resource where t_sys_role_resource.res_id=t_sys_resource.id and exists (select 1 from t_sys_user_role where t_sys_user_role.role_id=t_sys_role_resource.role_id and t_sys_user_role.user_id=#para(userId)))
	#end	
	#order()
#end
### 系统配置列表
#sql("config.list")
	select * from t_sys_config
	
	where 1 = 1
	#if(keyword)
		and (name like #like(keyword) or code like #like(keyword) or remark like #like(keyword))
	#end
	#if(state)
		and state = #para(state)
	#end
	#if(type)
		and type = #para(type)
	#end	
	#order()
	
#end

### 系统代码表列表
#sql("dict.list")
	select * from t_sys_dict
	
	where 1 = 1
	#if(keyword)
		and (name like #like(keyword) or val like #like(keyword) or remark like #like(keyword))
	#end
	#if(state)
		and state = #para(state)
	#end
	#if(type)
		and type = #para(type)
	#end	
	#if(parent_val)
		and parent_id = (select id from t_sys_dict temp where temp.parent_id=0 and temp.val=#para(parent_val))
	#end	
	#if(null!=id)
		and parent_id = #para(id)
	#end
	
	#order()
	
#end

### 系统角色列表
#sql("role.list")
	select * from t_sys_role
	
	where 1 = 1
	#if(keyword)
		and name like #like(keyword)
	#end
	#if(state)
		and state = #para(state)
	#end
	
	#order()
	
#end

### 系统机构列表
#sql("org.list")
	select * from t_sys_org
	
	where 1 = 1
	#if(keyword)
		and name like #like(keyword)
	#end
	#if(state)
		and state = #para(state)
	#end
	
	#if(null!=id)
		and parent_id = #para(id)
	#end
	
	#order()
	
#end

### 系统用户
#sql("user.list")
	select t_sys_user.*,t_sys_org.name org_name from t_sys_user left join t_sys_org on t_sys_user.org_id=t_sys_org.id
	
	where 1 = 1
	#if(keyword)
		and t_sys_user.name like #like(keyword)
	#end
	#if(state)
		and t_sys_user.state = #para(state)
	#end
	#if(orgId)
		and t_sys_org.path like #like(","+orgId+",")
	#end
	#if(deptId=null)
		and org_id =  #para(deptId)
	#end	
	#order()
	
#end

### 系统定时任务列表
#sql("schedule.list")
	select * from t_sys_schedule
	
	where 1 = 1
	#if(keyword)
		and (name like #like(keyword) or code like #like(keyword) or remark like #like(keyword))
	#end
	#if(state)
		and state = #para(state)
	#end
	#if(type)
		and type = #para(type)
	#end	
	#order()
	
#end

### 区域管理列表
#sql("area.list")
	select * from t_sys_area
	
	where 1 = 1
	#if(keyword)
		and (fullname like #like(keyword) or code like #like(keyword) or phone like #like(keyword) or zip like #like(keyword))
	#end
	
	#if(state)
		and state = #para(state)
	#end
	
	#if(null!=id)
		and parent_id = #para(id)
	#end
	
	#order()
	
#end

### 系统附件表列表
#sql("atta.list")
	select * from t_sys_atta
	
	where 1 = 1
	#if(keyword)
		and name like #like(keyword)
	#end
	#if(type)
		and data_type = #para(type)
	#end
	
	#if(null!=id)
		and data_id = #para(id)
	#end
	
	#if(stime)
		and ctime >= #para(stime)
	#end	
	#if(etime)
		and ctime <= #para(etime)
	#end
	
	#if(null!=lastId)
		and id > #para(lastId)
	#end
	#if(null!=extArray)
	 	and  ext in ( #for(ext:extArray)#(for.index>0?",":"") '#(ext)' #end )
	#end
	#if(null!=typeArray)
	 	and  data_type in ( #for(ext:typeArray)#(for.index>0?",":"") '#(ext)' #end )
	#end
	
	#order()
	
#end

#sql("atta.update.data")
	update t_sys_atta set data_id=#para(id) where data_type=#para(type) and id in ( #for(id:idArray)#(for.index>0?",":"") #(id) #end )
#end