package com.yj.auto.core.jfinal.interceptor;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.yj.auto.core.base.exception.AutoException;

public class ExceptionInterceptor implements Interceptor {
	// private static final Log logger = Log.getLog(ExceptionInterceptor.class);

	@Override
	public void intercept(Invocation ai) {
		try {
			ai.invoke();
		} catch (Exception e) {
			Controller c = ai.getController();
			HttpServletRequest request = c.getRequest();
			if (e instanceof AutoException) {// 业务异常处理(预留)

			}
			// if (NetUtils.isAjax(request)) {
			// ResponseModel<String> res = new ResponseModel<String>(false);
			// res.setMsg("系统异常:" + e.getMessage());
			// c.renderJson(res);
			// return;
			// }
			throw e;
		}
	}
}
