package com.yj.auto.core.web.system.model.bean;

import java.util.Date;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;

import com.yj.auto.core.jfinal.base.*;

/**
 *  系统用户角色组
 */
@SuppressWarnings("serial")
public abstract class UserRoleEntity<M extends UserRoleEntity<M>> extends BaseEntity<M> {
	
	public static final String TABLE_NAME = "t_sys_user_role"; //数据表名称
	
	public static final String TABLE_PK = "id"; //数据表主键
	
	public static final String TABLE_REMARK = "系统用户角色组"; //数据表备注

	public String getTableName(){
		return TABLE_NAME;
	}

	public String getTableRemark(){
		return TABLE_REMARK;
	}
	
	public String getTablePK(){
		return TABLE_PK;
	}
	
   		
	/**
	 * Column ：id
	 * @return 主键
	 */
   		
	public Integer getId(){
   		return get("id");
   	}
	
	public void setId(Integer id){
   		set("id" , id);
   	}	
   		
	/**
	 * Column ：user_id
	 * @return 用户主键
	 */
   	@NotNull 	
	public Integer getUserId(){
   		return get("user_id");
   	}
	
	public void setUserId(Integer userId){
   		set("user_id" , userId);
   	}	
   		
	/**
	 * Column ：role_id
	 * @return 角色主键
	 */
   	@NotNull 	
	public Integer getRoleId(){
   		return get("role_id");
   	}
	
	public void setRoleId(Integer roleId){
   		set("role_id" , roleId);
   	}	
}