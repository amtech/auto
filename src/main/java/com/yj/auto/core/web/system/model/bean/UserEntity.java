package com.yj.auto.core.web.system.model.bean;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.yj.auto.core.jfinal.base.BaseEntity;
import com.yj.auto.core.web.system.model.validator.UserEditGroup;
import com.yj.auto.core.web.system.model.validator.UserSetGroup;

/**
 * 系统用户
 */
@SuppressWarnings("serial")
public abstract class UserEntity<M extends UserEntity<M>> extends BaseEntity<M> {

	public static final String TABLE_NAME = "t_sys_user"; // 数据表名称

	public static final String TABLE_PK = "id"; // 数据表主键

	public static final String TABLE_REMARK = "系统用户"; // 数据表备注

	public String getTableName() {
		return TABLE_NAME;
	}

	public String getTableRemark() {
		return TABLE_REMARK;
	}

	public String getTablePK() {
		return TABLE_PK;
	}

	/**
	 * Column ：id
	 * 
	 * @return 用户主键
	 */

	public Integer getId() {
		return get("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * Column ：org_id
	 * 
	 * @return 机构主键
	 */

	public Integer getOrgId() {
		return get("org_id");
	}

	public void setOrgId(Integer orgId) {
		set("org_id", orgId);
	}

	/**
	 * Column ：code
	 * 
	 * @return 登录账号
	 */
	@NotBlank(groups = { UserEditGroup.class })
	@Length(max = 64, groups = { UserEditGroup.class, UserSetGroup.class })
	public String getCode() {
		return get("code");
	}

	public void setCode(String code) {
		set("code", code);
	}

	/**
	 * Column ：name
	 * 
	 * @return 用户名称
	 */
	@NotBlank(groups = { UserSetGroup.class })
	@Length(max = 64, groups = { UserEditGroup.class, UserSetGroup.class })
	public String getName() {
		return get("name");
	}

	public void setName(String name) {
		set("name", name);
	}

	/**
	 * Column ：pwd
	 * 
	 * @return 登录密码
	 */
	@Length(max = 16, groups = { UserEditGroup.class, UserSetGroup.class })
	public String getPwd() {
		return get("pwd");
	}

	public void setPwd(String pwd) {
		set("pwd", pwd);
	}

	/**
	 * Column ：tel
	 * 
	 * @return 电话号码
	 */
	@Length(max = 32, groups = { UserEditGroup.class, UserSetGroup.class })
	public String getTel() {
		return get("tel");
	}

	public void setTel(String tel) {
		set("tel", tel);
	}

	/**
	 * Column ：mobile
	 * 
	 * @return 手机号码
	 */
	@Length(max = 32, groups = { UserEditGroup.class, UserSetGroup.class })
	public String getMobile() {
		return get("mobile");
	}

	public void setMobile(String mobile) {
		set("mobile", mobile);
	}

	/**
	 * Column ：email
	 * 
	 * @return 电子邮件
	 */
	@Length(max = 32, groups = { UserEditGroup.class, UserSetGroup.class })
	public String getEmail() {
		return get("email");
	}

	public void setEmail(String email) {
		set("email", email);
	}

	/**
	 * Column ：card
	 * 
	 * @return 身份证
	 */
	@Length(max = 32, groups = { UserEditGroup.class, UserSetGroup.class })
	public String getCard() {
		return get("card");
	}

	public void setCard(String card) {
		set("card", card);
	}

	/**
	 * Column ：state
	 * 
	 * @return 用户状态
	 */
	@NotBlank(groups = { UserEditGroup.class })
	@Length(max = 32, groups = { UserEditGroup.class, UserSetGroup.class })
	public String getState() {
		return get("state");
	}

	public void setState(String state) {
		set("state", state);
	}

	/**
	 * Column ：login_ip
	 * 
	 * @return 最后登录IP
	 */
	@Length(max = 32)
	public String getLoginIp() {
		return get("login_ip");
	}

	public void setLoginIp(String loginIp) {
		set("login_ip", loginIp);
	}

	/**
	 * Column ：login_time
	 * 
	 * @return 最后登录时间
	 */

	public Date getLoginTime() {
		return get("login_time");
	}

	public void setLoginTime(Date loginTime) {
		set("login_time", loginTime);
	}

	/**
	 * Column ：sort
	 * 
	 * @return 排序号
	 */
	@NotNull(groups = { UserEditGroup.class })
	public Integer getSort() {
		return get("sort");
	}

	public void setSort(Integer sort) {
		set("sort", sort);
	}

	/**
	 * Column ：remark
	 * 
	 * @return 描述
	 */
	@Length(max = 1024, groups = { UserEditGroup.class, UserSetGroup.class })
	public String getRemark() {
		return get("remark");
	}

	public void setRemark(String remark) {
		set("remark", remark);
	}

	/**
	 * Column ：param1
	 * 
	 * @return 参数1
	 */
	@Length(max = 512, groups = { UserEditGroup.class, UserSetGroup.class })
	public String getParam1() {
		return get("param1");
	}

	public void setParam1(String param1) {
		set("param1", param1);
	}

	/**
	 * Column ：param2
	 * 
	 * @return 参数2
	 */
	@Length(max = 512, groups = { UserEditGroup.class, UserSetGroup.class })
	public String getParam2() {
		return get("param2");
	}

	public void setParam2(String param2) {
		set("param2", param2);
	}

	/**
	 * Column ：param3
	 * 
	 * @return 参数3
	 */
	@Length(max = 512, groups = { UserEditGroup.class, UserSetGroup.class })
	public String getParam3() {
		return get("param3");
	}

	public void setParam3(String param3) {
		set("param3", param3);
	}

	/**
	 * Column ：ltime
	 * 
	 * @return 最后修改时间
	 */

	public Date getLtime() {
		return get("ltime");
	}

	public void setLtime(Date ltime) {
		set("ltime", ltime);
	}

	/**
	 * Column ：luser
	 * 
	 * @return 最后修改人
	 */

	public Integer getLuser() {
		return get("luser");
	}

	public void setLuser(Integer luser) {
		set("luser", luser);
	}
}