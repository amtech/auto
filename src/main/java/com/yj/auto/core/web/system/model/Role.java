package com.yj.auto.core.web.system.model;

import java.util.List;

import com.yj.auto.core.base.annotation.Table;
import com.yj.auto.core.web.system.model.bean.RoleEntity;

/**
 * 系统角色
 */
@SuppressWarnings("serial")
@Table(name = Role.TABLE_NAME, key = Role.TABLE_PK, remark = Role.TABLE_REMARK)
public class Role extends RoleEntity<Role> {

	public List<RoleResource> resources;// 资源明细

	public List<RoleResource> getResources() {
		return resources;
	}

	public void setResources(List<RoleResource> resources) {
		this.resources = resources;
	}

}