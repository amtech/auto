package com.yj.auto.core.web.common.controller;

import com.jfinal.core.ActionKey;
import com.jfinal.log.Log;
import com.yj.auto.core.jfinal.base.BaseController;

/**
 * 
 */
public class CacheController extends BaseController {
	private static Log logger = Log.getLog(CacheController.class);

	@ActionKey("/dc")
	public void dict() {
		// DictService.me
	}

	@ActionKey("/dc/children")
	public void children() {
		// DictService.me
	}
}