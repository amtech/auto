package com.yj.auto.core.web.log.model.bean;

import java.util.Date;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;

import com.yj.auto.core.jfinal.base.*;

/**
 *  访问日志
 */
@SuppressWarnings("serial")
public abstract class AccessLogEntity<M extends AccessLogEntity<M>> extends BaseEntity<M> {
	
	public static final String TABLE_NAME = "t_log_access"; //数据表名称
	
	public static final String TABLE_PK = "id"; //数据表主键
	
	public static final String TABLE_REMARK = "访问日志"; //数据表备注

	public String getTableName(){
		return TABLE_NAME;
	}

	public String getTableRemark(){
		return TABLE_REMARK;
	}
	
	public String getTablePK(){
		return TABLE_PK;
	}
	
   		
	/**
	 * Column ：id
	 * @return 主键
	 */
   		
	public Integer getId(){
   		return get("id");
   	}
	
	public void setId(Integer id){
   		set("id" , id);
   	}	
   		
	/**
	 * Column ：user_id
	 * @return 用户主键
	 */
   		
	public Integer getUserId(){
   		return get("user_id");
   	}
	
	public void setUserId(Integer userId){
   		set("user_id" , userId);
   	}	
   		
	/**
	 * Column ：user_code
	 * @return 用户代码
	 */
   	@Length(max = 64)	
	public String getUserCode(){
   		return get("user_code");
   	}
	
	public void setUserCode(String userCode){
   		set("user_code" , userCode);
   	}	
   		
	/**
	 * Column ：user_name
	 * @return 用户名称
	 */
   	@Length(max = 64)	
	public String getUserName(){
   		return get("user_name");
   	}
	
	public void setUserName(String userName){
   		set("user_name" , userName);
   	}	
   		
	/**
	 * Column ：client_ip
	 * @return 客户端IP
	 */
   	@Length(max = 32)	
	public String getClientIp(){
   		return get("client_ip");
   	}
	
	public void setClientIp(String clientIp){
   		set("client_ip" , clientIp);
   	}	
   		
	/**
	 * Column ：server_ip
	 * @return 服务器IP
	 */
   	@Length(max = 32)	
	public String getServerIp(){
   		return get("server_ip");
   	}
	
	public void setServerIp(String serverIp){
   		set("server_ip" , serverIp);
   	}	
   		
	/**
	 * Column ：target
	 * @return 访问功能
	 */
   	@Length(max = 256)	
	public String getTarget(){
   		return get("target");
   	}
	
	public void setTarget(String target){
   		set("target" , target);
   	}	
   		
	/**
	 * Column ：uri
	 * @return 目标URI
	 */
   	@NotBlank
	@Length(max = 256)	
	public String getUri(){
   		return get("uri");
   	}
	
	public void setUri(String uri){
   		set("uri" , uri);
   	}	
   		
	/**
	 * Column ：access_time
	 * @return 访问时间
	 */
   		
	public Date getAccessTime(){
   		return get("access_time");
   	}
	
	public void setAccessTime(Date accessTime){
   		set("access_time" , accessTime);
   	}	
   		
	/**
	 * Column ：query_string
	 * @return 查询参数
	 */
   	@Length(max = 65535)	
	public String getQueryString(){
   		return get("query_string");
   	}
	
	public void setQueryString(String queryString){
   		set("query_string" , queryString);
   	}	
   		
	/**
	 * Column ：params_map
	 * @return 提交参数JSON
	 */
   	@Length(max = 65535)	
	public String getParamsMap(){
   		return get("params_map");
   	}
	
	public void setParamsMap(String paramsMap){
   		set("params_map" , paramsMap);
   	}	
   		
	/**
	 * Column ：session_id
	 * @return 会话ID
	 */
   	@Length(max = 64)	
	public String getSessionId(){
   		return get("session_id");
   	}
	
	public void setSessionId(String sessionId){
   		set("session_id" , sessionId);
   	}	
   		
	/**
	 * Column ：cookie_id
	 * @return COOKIE_ID
	 */
   	@Length(max = 64)	
	public String getCookieId(){
   		return get("cookie_id");
   	}
	
	public void setCookieId(String cookieId){
   		set("cookie_id" , cookieId);
   	}	
   		
	/**
	 * Column ：ua
	 * @return UA
	 */
   	@Length(max = 65535)	
	public String getUa(){
   		return get("ua");
   	}
	
	public void setUa(String ua){
   		set("ua" , ua);
   	}	
   		
	/**
	 * Column ：referer
	 * @return 页面跳转来源
	 */
   	@Length(max = 256)	
	public String getReferer(){
   		return get("referer");
   	}
	
	public void setReferer(String referer){
   		set("referer" , referer);
   	}	
   		
	/**
	 * Column ：channel
	 * @return 访问渠道
	 */
   	@Length(max = 32)	
	public String getChannel(){
   		return get("channel");
   	}
	
	public void setChannel(String channel){
   		set("channel" , channel);
   	}	
   		
	/**
	 * Column ：platform
	 * @return 访问平台
	 */
   	@Length(max = 32)	
	public String getPlatform(){
   		return get("platform");
   	}
	
	public void setPlatform(String platform){
   		set("platform" , platform);
   	}	
}