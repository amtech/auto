package com.yj.auto.core.web.log.controller;

import com.jfinal.log.Log;
import com.yj.auto.core.base.annotation.Controller;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.base.model.ResponseModel;
import com.yj.auto.core.base.online.OnlineContext;
import com.yj.auto.core.jfinal.base.BaseController;
import com.yj.auto.core.web.log.model.LoginLog;
import com.yj.auto.core.web.log.service.LoginLogService;
import com.yj.auto.plugin.table.model.DataTables;
import com.yj.auto.utils.DateUtil;

/**
 * 登录日志 管理
 * 
 * 描述：
 * 
 */
@Controller(viewPath = "log", key = "/log/login")
public class LoginLogController extends BaseController {
	private static final Log logger = Log.getLog(LoginLogController.class);

	private static final String RESOURCE_URI = "log/login/index";
	private static final String INDEX_PAGE = "login_index.html";
	private static final String SHOW_PAGE = "login_show.html";

	LoginLogService loginLogSrv = null;

	public void index() {
		QueryModel query = new QueryModel();
		query.setStime(DateUtil.toDateStr(DateUtil.addDay(DateUtil.date(), -7)));
		setAttr("query", query);
		render(INDEX_PAGE);
	}

	public void list(QueryModel query) {
		DataTables<LoginLog> dt = getDataTable(query, loginLogSrv);
		renderJson(dt);
	}

	public void get() {
		Integer id = getParaToInt(0);
		LoginLog model = loginLogSrv.get(id);
		setAttr("model", model);
		render(SHOW_PAGE);
	}

	public boolean delete() {
		ResponseModel<String> res = _delete(loginLogSrv);
		return res.isSuccess();
	}

	// 根据条件清空日志
	public boolean clear() {
		String keyword = getPara("keyword");
		String stime = getPara("stime");
		String etime = getPara("etime");
		boolean success = loginLogSrv.delete(keyword, stime, etime);
		ResponseModel<String> res = new ResponseModel<String>(success);
		res.setMsg("清空" + loginLogSrv.getDao().getTableRemark() + (res.isSuccess() ? "成功" : "失败"));
		renderJson(res);
		return success;
	}

	// 强制踢下线
	public void invalidate() {
		String sessionId = getPara();
		OnlineContext.invalidate(sessionId);
		ResponseModel<String> res = new ResponseModel<String>(true);
		res.setMsg("强制下线成功");
		renderJson(res);
	}
}