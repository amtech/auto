package com.yj.auto.core.web.log.controller;

import com.jfinal.log.Log;
import com.yj.auto.core.base.annotation.Controller;
import com.yj.auto.core.jfinal.base.BaseController;
import com.yj.auto.plugin.log4j.Log4jAsyncWriter;

@Controller(viewPath = "log", key = "/log")
public class SysLogController extends BaseController {
	private static final String LOG4J_PAGE = "log4j.html";
	private static final String DRUID_PAGE = "druid.html";

	public void log4j() {
		render(LOG4J_PAGE);
	}

	public void druid() {
		 Log.getLog(SysLogController.class).error("aabbcc");
		render(DRUID_PAGE);
	}
}