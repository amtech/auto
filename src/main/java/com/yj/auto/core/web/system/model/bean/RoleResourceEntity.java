package com.yj.auto.core.web.system.model.bean;

import java.util.Date;

import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Length;

import com.yj.auto.core.jfinal.base.*;

/**
 * 系统角色资源权限
 */
@SuppressWarnings("serial")
public abstract class RoleResourceEntity<M extends RoleResourceEntity<M>> extends BaseEntity<M> {

	public static final String TABLE_NAME = "t_sys_role_resource"; // 数据表名称

	public static final String TABLE_PK = "id"; // 数据表主键

	public static final String TABLE_REMARK = "系统角色资源权限"; // 数据表备注

	public String getTableName() {
		return TABLE_NAME;
	}

	public String getTableRemark() {
		return TABLE_REMARK;
	}

	public String getTablePK() {
		return TABLE_PK;
	}

	/**
	 * Column ：id
	 * 
	 * @return 主键
	 */

	public Integer getId() {
		return get("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * Column ：role_id
	 * 
	 * @return 系统角_角色主键
	 */
	@NotNull
	public Integer getRoleId() {
		return get("role_id");
	}

	public void setRoleId(Integer roleId) {
		set("role_id", roleId);
	}

	/**
	 * Column ：res_id
	 * 
	 * @return 资源主键
	 */
	@NotNull
	public Integer getResId() {
		return get("res_id");
	}

	public void setResId(Integer resId) {
		set("res_id", resId);
	}

	/**
	 * Column ：res_code
	 * 
	 * @return 资源编号
	 */
	@Length(max = 64)
	public String getResCode() {
		return get("res_code");
	}

	public void setResCode(String resCode) {
		set("res_code", resCode);
	}
}