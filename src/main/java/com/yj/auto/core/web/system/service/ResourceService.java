package com.yj.auto.core.web.system.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yj.auto.Constants;
import com.yj.auto.core.base.annotation.Service;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.jfinal.base.BaseCache;
import com.yj.auto.core.jfinal.base.BaseService;
import com.yj.auto.core.web.system.model.Resource;

/**
 * Resource 管理 描述：
 */
@Service(name = "resourceSrv")
public class ResourceService extends BaseService<Resource> implements BaseCache {

	private static final Log logger = Log.getLog(ResourceService.class);

	public static final String SQL_LIST = "system.resource.list";

	public static final Resource dao = new Resource().dao();

	@Override
	public Resource getDao() {
		return dao;
	}

	public boolean save(Resource model) {
		boolean success = model.save();
		if (success) {
			updatePath(model);
		}
		return success;
	}

	public boolean update(Resource model) {
		boolean success = model.update();
		if (success) {
			updatePath(model);
		}
		return success;
	}

	private void updatePath(Resource model) {
		Resource temp = model;
		String path = "," + model.getId() + ",";
		while (temp.getParentId() != 0) {
			Resource parent = this.get(temp.getParentId());
			if (null == parent) {
				break;
			} else {
				path = "," + parent.getId() + path;
			}
			temp = parent;
		}
		model.setPath(path);
		model.update();
	}

	@Override
	public SqlPara getListSqlPara(QueryModel query) {
		if (StrKit.isBlank(query.getOrderby())) {
			query.setOrderby("sort");
		}
		return getSqlPara(SQL_LIST, query);
	}

	public List<Resource> children(Integer id, String state) {
		if (null == id)
			id = new Integer(0);
		QueryModel query = new QueryModel();
		query.setId(id);
		query.setState(state);
		query.setOrderby("sort");
		return find(SQL_LIST, query);
	}

	public List<Resource> children(Integer id, Integer userId) {
		if (null == id)
			id = new Integer(0);
		QueryModel query = new QueryModel();
		query.setId(id);
		query.setState(Constants.DATA_STATE_VALID);
		query.setUserId(userId);
		query.setOrderby("sort");
		return find(SQL_LIST, query);
	}

	public boolean updateSort(Integer[] id) {
		if (null == id || id.length < 1)
			return false;
		List<Resource> list = new ArrayList<Resource>();
		for (int i = 0; i < id.length; i++) {
			Resource d = new Resource();
			d.setId(id[i]);
			d.setSort(i + 1);
			list.add(d);
		}
		String sql = "update t_sys_resource set sort=? where id=?";
		int[] result = Db.batch(sql, "sort, id", list, 500);
		return result.length > 0;
	}

	// 根据URI查询
	public Resource getByUri(String uri) {
		if (StrKit.isBlank(uri))
			return null;
		String sql = "select * from t_sys_resource where uri=?";
		return dao.findFirst(sql, uri);
	}

	@Override
	public void loadCache() {
		QueryModel query = new QueryModel();
		query.setOrderby("sort");
		query.setState(Constants.DATA_STATE_VALID);
		List<Resource> list = find(SQL_LIST, query);
		for (Resource r : list) {
			r.setIsParent(children(r.getId(), Constants.DATA_STATE_VALID).size() > 0);
			addCache(r.getId(), r);
		}
	}

	@Override
	public String getCacheName() {
		return Constants.CACHE_NAME_RESOURCE;
	}

	public Resource getCacheByURI(String uri) {
		if (StrKit.isBlank(uri)) {
			return null;
		}
		List<Resource> list = this.getCaches();
		for (Resource res : list) {
			if (uri.equals(res.getUri())) {
				return res;
			}
		}
		return null;
	}
}