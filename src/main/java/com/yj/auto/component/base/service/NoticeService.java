package com.yj.auto.component.base.service;

import com.jfinal.kit.*;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.*;
import com.yj.auto.core.base.model.*;
import com.yj.auto.core.jfinal.base.*;
import com.yj.auto.helper.LogHelper;
import com.yj.auto.core.base.annotation.*;
import com.yj.auto.Constants;
import com.yj.auto.component.base.model.*;

/**
 * Notice 管理 描述：
 */
@Service(name = "noticeSrv")
public class NoticeService extends BaseService<Notice> {

	private static final Log logger = Log.getLog(NoticeService.class);

	public static final String SQL_LIST = "component.base.notice.list";
	public static final String ATTA_NOTICE = "notice_atta";

	public static final Notice dao = new Notice().dao();

	@Override
	public Notice getDao() {
		return dao;
	}

	@Override
	public SqlPara getListSqlPara(QueryModel query) {
		if (StrKit.isBlank(query.getOrderby())) {
			query.setOrderby("stime desc");
		}
		return getSqlPara(SQL_LIST, query);
	}

	public boolean delete(Integer... ids) {
		LogHelper.delReadingLog(dao.getTableName(), ids);
		return super.delete(ids, ATTA_NOTICE);
	}
}