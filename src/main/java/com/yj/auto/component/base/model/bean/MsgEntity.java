package com.yj.auto.component.base.model.bean;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.yj.auto.component.base.model.validator.MsgDraftGroup;
import com.yj.auto.component.base.model.validator.MsgSentGroup;
import com.yj.auto.core.jfinal.base.BaseEntity;

/**
 * 系统消息
 */
@SuppressWarnings("serial")
public abstract class MsgEntity<M extends MsgEntity<M>> extends BaseEntity<M> {

	public static final String TABLE_NAME = "t_com_msg"; // 数据表名称

	public static final String TABLE_PK = "id"; // 数据表主键

	public static final String TABLE_REMARK = "系统消息"; // 数据表备注

	public String getTableName() {
		return TABLE_NAME;
	}

	public String getTableRemark() {
		return TABLE_REMARK;
	}

	public String getTablePK() {
		return TABLE_PK;
	}

	/**
	 * Column ：id
	 * 
	 * @return 消息主键
	 */

	public Integer getId() {
		return get("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * Column ：subject
	 * 
	 * @return 消息主题
	 */
	@NotBlank(groups = { MsgDraftGroup.class, MsgSentGroup.class })
	@Length(max = 256)
	public String getSubject() {
		return get("subject");
	}

	public void setSubject(String subject) {
		set("subject", subject);
	}

	/**
	 * Column ：ctime
	 * 
	 * @return 发送时间
	 */
	public Date getCtime() {
		return get("ctime");
	}

	public void setCtime(Date ctime) {
		set("ctime", ctime);
	}

	/**
	 * Column ：from_id
	 * 
	 * @return 发件人ID
	 */
	public Integer getFromId() {
		return get("from_id");
	}

	public void setFromId(Integer fromId) {
		set("from_id", fromId);
	}

	/**
	 * Column ：to_id
	 * 
	 * @return 接收人主键
	 */
	@NotBlank(groups = { MsgSentGroup.class })
	@Length(max = 512)
	public String getToId() {
		return get("to_id");
	}

	public void setToId(String toId) {
		set("to_id", toId);
	}

	/**
	 * Column ：to_name
	 * 
	 * @return 接收人名称
	 */
	@Length(max = 1024)
	@NotBlank(groups = { MsgSentGroup.class })
	public String getToName() {
		return get("to_name");
	}

	public void setToName(String toName) {
		set("to_name", toName);
	}

	/**
	 * Column ：cc_id
	 * 
	 * @return 抄送主键
	 */
	@Length(max = 512)
	public String getCcId() {
		return get("cc_id");
	}

	public void setCcId(String ccId) {
		set("cc_id", ccId);
	}

	/**
	 * Column ：cc_name
	 * 
	 * @return 抄送名称
	 */
	@Length(max = 1024)
	public String getCcName() {
		return get("cc_name");
	}

	public void setCcName(String ccName) {
		set("cc_name", ccName);
	}

	/**
	 * Column ：flag
	 * 
	 * @return 邮件重要性
	 */
	@Length(max = 32)
	public String getFlag() {
		return get("flag");
	}

	public void setFlag(String flag) {
		set("flag", flag);
	}

	/**
	 * Column ：atta
	 * 
	 * @return 是否有附件
	 */
	@Length(max = 32)
	public String getAtta() {
		return get("flag");
	}

	public void setAtta(String atta) {
		set("atta", atta);
	}

	/**
	 * Column ：receipt
	 * 
	 * @return 是否需要回执
	 */
	@Length(max = 32)
	public String getReceipt() {
		return get("receipt");
	}

	public void setReceipt(String receipt) {
		set("receipt", receipt);
	}

	/**
	 * Column ：content
	 * 
	 * @return 消息内容
	 */
	@Length(max = 65535)
	public String getContent() {
		return get("content");
	}

	public void setContent(String content) {
		set("content", content);
	}

	/**
	 * Column ：param1
	 * 
	 * @return 参数1
	 */
	@Length(max = 512)
	public String getParam1() {
		return get("param1");
	}

	public void setParam1(String param1) {
		set("param1", param1);
	}

	/**
	 * Column ：param2
	 * 
	 * @return 参数2
	 */
	@Length(max = 512)
	public String getParam2() {
		return get("param2");
	}

	public void setParam2(String param2) {
		set("param2", param2);
	}

	/**
	 * Column ：param3
	 * 
	 * @return 参数3
	 */
	@Length(max = 512)
	public String getParam3() {
		return get("param3");
	}

	public void setParam3(String param3) {
		set("param3", param3);
	}
}