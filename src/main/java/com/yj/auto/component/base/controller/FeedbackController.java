package com.yj.auto.component.base.controller;

import java.util.*;

import com.jfinal.core.ActionKey;
import com.jfinal.kit.*;
import com.jfinal.log.Log;
import com.yj.auto.core.base.model.*;
import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.jfinal.base.*;
import com.yj.auto.core.web.system.model.User;
import com.yj.auto.helper.AutoHelper;
import com.yj.auto.plugin.table.model.DataTables;
import com.yj.auto.Constants;
import com.yj.auto.component.base.model.*;
import com.yj.auto.component.base.service.*;

/**
 * 系统反馈 管理
 * 
 * 描述：
 * 
 */
@Controller(viewPath = "component/base")
public class FeedbackController extends BaseController {
	private static final Log logger = Log.getLog(FeedbackController.class);

	private static final String RESOURCE_URI = "feedback/index";
	private static final String INDEX_PAGE = "feedback_index.html";
	private static final String ADD_PAGE = "feedback_add.html";
	private static final String EDIT_PAGE = "feedback_edit.html";
	private static final String SHOW_PAGE = "feedback_show.html";

	FeedbackService feedbackSrv = null;

	public void index() {
		render(INDEX_PAGE);
	}

	public void list(QueryModel query) {
		DataTables<Feedback> dt = getDataTable(query, feedbackSrv);
		renderJson(dt);
	}

	public void get() {
		Integer id = getParaToInt(0);
		Feedback model = feedbackSrv.get(id);
		if (model.getReplyId() != null) {
			User user = AutoHelper.getUserService().get(model.getReplyId());
			if (null != user) {
				model.put("replyUser", user);
			}
		}
		setAttr("model", model);
		render(SHOW_PAGE);
	}

	@ActionKey("/index/feedback")
	public void add() {
		Feedback model = new Feedback();
		model.setState(Constants.DATA_STATE_VALID);
		setAttr("model", model);
		render(ADD_PAGE);
	}

	@ActionKey("/index/feedbackSave")
	@Valid(type = Feedback.class)
	public boolean saveOrUpdateIndex(Feedback model) {
		return this.saveOrUpdate(model);
	}

	public void form() {
		Integer id = getParaToInt(0);
		Feedback model = feedbackSrv.get(id);
		setAttr("model", model);
		render(EDIT_PAGE);
	}

	@Valid(type = Feedback.class)
	public boolean saveOrUpdate(Feedback model) {
		model.setLuser(getSuId());
		model.setLtime(Constants.NOW());
		boolean success = false;
		if (null == model.getId()) {
			model.setUserId(model.getLuser());
			model.setCtime(model.getLtime());
			success = feedbackSrv.save(model);
		} else {
			model.setReplyId(model.getLuser());
			model.setReplyTime(model.getLtime());
			model.setState("02");
			success = feedbackSrv.update(model);
		}
		ResponseModel<String> res = renderSaveOrUpdate(success, feedbackSrv);
		return res.isSuccess();
	}

	public boolean delete() {
		ResponseModel<String> res = _delete(feedbackSrv);
		return res.isSuccess();
	}
}