package com.yj.auto.component.base.controller;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.log.Log;
import com.yj.auto.Constants;
import com.yj.auto.component.base.model.DocType;
import com.yj.auto.component.base.service.DocTypeService;
import com.yj.auto.core.base.annotation.Controller;
import com.yj.auto.core.base.annotation.Valid;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.base.model.ResponseModel;
import com.yj.auto.core.jfinal.base.BaseController;
import com.yj.auto.plugin.table.model.DataTables;
import com.yj.auto.plugin.ztree.ZTreeNode;

/**
 * 知识库类型 管理
 * 
 * 描述：
 * 
 */
@Controller(viewPath = "component/base")
public class DocTypeController extends BaseController {
	private static final Log logger = Log.getLog(DocTypeController.class);

	private static final String RESOURCE_URI = "doc_type/index";
	private static final String INDEX_PAGE = "doc_type_index.html";
	private static final String FORM_PAGE = "doc_type_form.html";
	private static final String SHOW_PAGE = "doc_type_show.html";

	DocTypeService docTypeSrv = null;

	public void index() {
		render(INDEX_PAGE);
	}

	public void list(QueryModel query) {
		DataTables<DocType> dt = getDataTable(query, docTypeSrv);
		renderJson(dt);
	}

	public void children() {
		Integer id = getParaToInt("id");
		String state = getPara("state");
		List<DocType> list = docTypeSrv.children(id, state);
		List<ZTreeNode> result = new ArrayList<ZTreeNode>();
		for (DocType root : list) {
			List<DocType> subs = docTypeSrv.children(root.getId(), state);
			ZTreeNode t = getZTreeNode(root, subs.size() > 0);
			if (null == id && t.getIsParent()) {
				t.setOpen(true);
				for (DocType obj : subs) {
					ZTreeNode st = getZTreeNode(obj, docTypeSrv.children(obj.getId(), state).size() > 0);
					t.addChildren(st);
				}
			}
			result.add(t);
		}
		renderJson(result);
	}

	private ZTreeNode getZTreeNode(DocType c, boolean parent) {
		ZTreeNode t = new ZTreeNode(c.getId().toString(), c.getName());
		t.setNocheck(false);
		t.setIsParent(parent);
		return t;
	}

	public boolean updateSort() {
		Integer[] id = getParaValuesToInt();
		boolean success = docTypeSrv.updateSort(id);
		ResponseModel<String> res = new ResponseModel<String>(success);
		renderJson(res);
		return success;
	}

	public void form() throws Exception {
		String id = getPara(0);
		DocType model = null;
		if (id.startsWith("new_")) {
			model = new DocType();
			model.setParentId(getParaToInt(1));
			model.setSort(getParaToInt(2));
			model.setName(getParaDecode(3));
		} else {
			model = docTypeSrv.get(Integer.parseInt(id));
		}
		if (null != model.getParentId()) {
			DocType parent = docTypeSrv.get(model.getParentId());
			if (null != parent) {
				setAttr("parent", parent);
			}
		}
		setAttr("model", model);
		render(FORM_PAGE);
	}

	@Valid(type = DocType.class)
	public boolean saveOrUpdate(DocType model) {
		model.setLuser(getSuId());
		model.setLtime(Constants.NOW());
		boolean success = false;
		if (null == model.getId()) {
			success = docTypeSrv.save(model);
		} else {
			success = docTypeSrv.update(model);
		}
		ResponseModel<String> res = new ResponseModel<String>(success);
		res.setData(model.getId().toString());
		res.setMsg("保存" + docTypeSrv.getDao().getTableRemark() + (res.isSuccess() ? "成功" : "失败"));
		renderJson(res);
		return success;
	}

	public boolean delete() {
		ResponseModel<String> res = _delete(docTypeSrv);
		return res.isSuccess();
	}
}