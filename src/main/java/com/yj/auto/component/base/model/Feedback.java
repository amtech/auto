package com.yj.auto.component.base.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.component.base.model.bean.*;
/**
 * 系统反馈
 */
@SuppressWarnings("serial")
@Table(name = Feedback.TABLE_NAME, key = Feedback.TABLE_PK, remark = Feedback.TABLE_REMARK)
public class Feedback extends FeedbackEntity<Feedback> {

}