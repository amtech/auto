package com.yj.auto.plugin.license;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class AutoLicense {
	public final static String KEY_ALGORITHM = "RSA";
	public final static String SIGNATURE_ALGORITHM = "MD5withRSA";
	private byte[] seed = "_Auto_1_License_2_Seed_".getBytes();
	private byte[] publicKey;
	private byte[] privateKey;
	private byte[] signature;

	public AutoLicense() {

	}

	public AutoLicense(String seed) {
		if (null != seed && seed.length() > 0) {
			this.seed = seed.getBytes();
		}
	}

	public AutoLicense(byte[] privateKey) {
		this.privateKey = privateKey;
	}

	public AutoLicense(byte[] publicKey, byte[] signature) {
		this.publicKey = publicKey;
		this.signature = signature;
	}

	public void generaterKey() throws Exception {
		KeyPairGenerator keygen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
		SecureRandom secrand = new SecureRandom();
		secrand.setSeed(getSeed()); // 初始化随机产生器
		keygen.initialize(1024, secrand);
		KeyPair keys = keygen.genKeyPair();
		PublicKey pubkey = keys.getPublic();
		PrivateKey prikey = keys.getPrivate();
		publicKey = Base64.getEncoder().encode(pubkey.getEncoded());
		privateKey = Base64.getEncoder().encode(prikey.getEncoded());
	}

	public byte[] sign(String text) throws Exception {
		PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(getPrivateKey()));
		KeyFactory keyf = KeyFactory.getInstance(KEY_ALGORITHM);
		PrivateKey prikey = keyf.generatePrivate(priPKCS8);
		// 用私钥对信息生成数字签名
		Signature signet = Signature.getInstance(SIGNATURE_ALGORITHM);
		signet.initSign(prikey);
		signet.update(text.getBytes());
		byte[] signed = Base64.getEncoder().encode(signet.sign());
		return signed;
	}

	public boolean verify(String text) throws Exception {
		if (null == getPublicKey() || getPublicKey().length < 1) {
			throw new Exception("PublicKey is not null!");
		}
		if (null == getSignature() || getSignature().length < 1) {
			throw new Exception("Signature is not null!");
		}
		// 解密由base64编码的公钥,并构造X509EncodedKeySpec对象
		X509EncodedKeySpec bobPubKeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(getPublicKey()));
		// RSA对称加密算法
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		// 取公钥匙对象
		PublicKey pubKey = keyFactory.generatePublic(bobPubKeySpec);
		// 解密由base64编码的数字签名
		byte[] signed = Base64.getDecoder().decode(getSignature());
		Signature signatureChecker = Signature.getInstance(SIGNATURE_ALGORITHM);
		signatureChecker.initVerify(pubKey);
		signatureChecker.update(text.getBytes());
		// 验证签名是否正常
		return signatureChecker.verify(signed);
	}

	public String getText() throws Exception {
		if (null == getPublicKey() || getPublicKey().length < 1) {
			throw new Exception("PublicKey is not null!");
		}
		if (null == getSignature() || getSignature().length < 1) {
			throw new Exception("Signature is not null!");
		}
		StringBuffer sb = new StringBuffer();
		sb.append(new String(getPublicKey()));
		sb.append(";");
		sb.append(new String(getSignature()));
		return sb.toString();
	}

	public byte[] getSeed() {
		return seed;
	}

	public void setSeed(byte[] seed) {
		this.seed = seed;
	}

	public byte[] getSignature() {
		return signature;
	}

	public byte[] getPublicKey() {
		return publicKey;
	}

	public byte[] getPrivateKey() {
		return privateKey;
	}
}