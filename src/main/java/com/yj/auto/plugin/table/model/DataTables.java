package com.yj.auto.plugin.table.model;

import java.util.ArrayList;
import java.util.List;

import com.yj.auto.Constants;

/** 
 */
public class DataTables<T> {

	// 请求传递参数
	private int draw = 0;
	private List<Columns> columns = new ArrayList<Columns>();
	private List<Order> order = new ArrayList<Order>();
	private int start;
	private int length = Constants.PAGE_SIZE;
	private Search search = new Search();

	// 相应传递参数
	private List<T> data;
	private int recordsTotal = 0;
	private int recordsFiltered = 0;

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public List<Columns> getColumns() {
		return columns;
	}

	public void setColumns(List<Columns> columns) {
		this.columns = columns;
	}

	public List<Order> getOrder() {
		return order;
	}

	public void setOrder(List<Order> order) {
		this.order = order;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public Search getSearch() {
		return search;
	}

	public void setSearch(Search search) {
		this.search = search;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	/**
	 * 根据每页数、开始记录数，计算当前页码
	 * 
	 * @return 当前页码
	 */
	public int getCurrentPage() {
		if (length < 1) {
			length = Constants.PAGE_SIZE;
		}
		int pageNumber = start / length;
		return pageNumber < 1 ? 1 : pageNumber + 1;
	}

	@Override
	public String toString() {
		return "DataTables [draw=" + draw + ", columns=" + columns.get(0).toString() + ", order=" + order.get(0).toString() + ", start=" + start + ", length=" + length + ", search=" + search + ", data=" + data + ", recordsTotal=" + recordsTotal + ", recordsFiltered=" + recordsFiltered + "]";
	}
}
