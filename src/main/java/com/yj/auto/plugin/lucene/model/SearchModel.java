package com.yj.auto.plugin.lucene.model;

import com.jfinal.json.FastJson;

public class SearchModel {
	private String field;

	private String keyword;

	public SearchModel(String field, String keyword) {
		super();
		this.field = field;
		this.keyword = keyword;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String toJson() {
		return FastJson.getJson().toJson(this);
	}
}
