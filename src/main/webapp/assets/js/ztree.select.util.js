(function($) {
	$.fn.zTreeSelect = function(options) {
		var $this = $(this);
		options.width=trim(options.width,$this.width());
		if(_.isNumber(options.width)&&options.width<250){
			options.width=250;
		}
		var setting = {
			containerObj : null,
			data : {
				simpleData : { enable : true }
			},
			async : {
				enable : true,
				url : options.uri,
				autoParam : _.isEmpty(options.autoParam) ? ["id"] : options.autoParam
			},
			view : {
				selectedMulti : options.multi
			},
			check : {
				enable : true,
				chkStyle : options.multi ? "checkbox" : "radio",
				radioType : "all",
				chkboxType : { "Y" : "", "N" : "" }
			},
			callback : {
				onClick : function (event, treeId, treeNode) {
					if(_.isFunction(options.onClick)){
						options.onClick.call(this,treeId,treeNode);
					}
				},
				onCheck : function (event, treeId, treeNode) {
					var panel=$("#"+treeId).parents("div.ztree-panel:first");
					var nid=jid(panel.data("node-id"));
					var nname=jid(panel.data("node-name"));
					if(options.multi){//多选
						var nidArr=_.isEmpty(nid.val())?[]:nid.val().split(";");
						var nnameArr=_.isEmpty(nname.val())?[]:nname.val().split(";");
						if(treeNode.checked){
							nidArr.push(treeNode.id);
							nnameArr.push(treeNode.name);
						}else{
							var idx=jQuery.inArray(treeNode.id,nidArr);
							if(idx>-1){
								nidArr.splice(idx, 1);
								nnameArr.splice(idx, 1);
							}
						}
						nid.val(nidArr.join(";"));
						nname.val(nnameArr.join(";"));
					}else{//单选
						if(treeNode.checked){
							nid.val(treeNode.id);
							nname.val(treeNode.name);
						}else{
							nid.val("");
							nname.val("");
						}
						panel.hide();
					}
					resetValidateField(nid);
					resetValidateField(nname);
					if(_.isFunction(options.onCheck)){
						options.onCheck.call(this,treeId,treeNode);
					}
				},
				onAsyncSuccess :function (event, treeId, treeNode, msg) {
					var treeObj = $.fn.zTree.getZTreeObj(treeId);
					var panel=$("#"+treeId).parents("div.ztree-panel:first");
					var nid=jid(panel.data("node-id"));
					var nidArr=_.isEmpty(nid.val())?[]:nid.val().split(";");
					for(var i = 0 ; i < nidArr.length; i++){
						var node = treeObj.getNodeByParam("id",nidArr[i], treeNode);
						treeObj.checkNode(node, true, true);
					}
				}
			}
		}
		
		var temp=jid($this.attr("id")+"_ztree_panel");
		if(temp.length>0){
			if(!temp.is(":visible"))temp.show();
			return $.fn.zTree.getZTreeObj(temp.find("ul.ztree").attr("id"));
		}
		
		var html="<div id='"+$this.attr("id")+"_ztree_panel' data-node-id='"+options.nodeId+"' data-node-name='"+options.nodeName+"' class='panel panel-default ztree-panel' style='position:absolute;z-index:9999;width:"+options.width+"px'>";
		if(options.searchAble){
			html+="<div class='panel-heading'><div class='input-group input-group-xs' >";
			html+="<input placeholder='请输入关键字'  class='form-control input-sm ztree-query-input'>";
			html+="<span class='input-group-addon' style='cursor:pointer;'> <i class='fa fa-search text-primary'></i>&nbsp;&nbsp;查询</span>";
			html+="</div></div>";
		}
		html+="<div class='panel-body' style='height:"+trim(options.height,"300px")+";overflow:auto;'>";
		html+="<ul id='"+guid()+"_ztree_ul' class='ztree'></ul>";
		html+="</div>";
		html+="<div class='panel-footer'><div class='btn-group btn-group-justified'>";
		html+="<a href='javascript:void(0)' class='btn btn-info ztree-close-btn' > <i class='fa fa-off text-danger'></i>&nbsp;&nbsp;关闭</a>";
		html+="<a href='javascript:void(0)' class='btn btn-info ztree-cancel-btn' > <i class='fa fa-minus-sign text-danger'></i>&nbsp;&nbsp;重置</a>";
		html+="</div></div>";
		html+="</div>";
		html=$(html);
		html.find(".ztree-close-btn").click(function(){
			var panel=$(this).parents("div.ztree-panel:first");
			jid(panel.data("node-name")).prop("readonly",false);
			panel.hide();
		});
		html.find(".ztree-cancel-btn").click(function(){
			var panel=$(this).parents("div.ztree-panel:first");
			jid(panel.data("node-id")).val("");
			jid(panel.data("node-name")).val("").prop("readonly",false);
			var ztree=$.fn.zTree.getZTreeObj(panel.find("ul.ztree").attr("id"));
			ztree.checkAllNodes(false);
			panel.hide();
		});
		setting.containerObj=html;
		$this.after(html);
		var offset=$this.position();
		html.css({"top":(offset.top+$this.outerHeight())+"px","left":offset.left+"px"});
		var ztree=$.fn.zTree.init(html.find("ul.ztree"), setting);
		return ztree;
	}

})(jQuery);
