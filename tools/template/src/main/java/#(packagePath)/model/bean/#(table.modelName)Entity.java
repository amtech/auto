package #(basepackage).model.bean;

import java.util.Date;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;
import com.yj.auto.core.jfinal.base.*;

/**
 *  #(table.remarks)
 */
@SuppressWarnings("serial")
public abstract class #(modelName)Entity<M extends #(modelName)Entity<M>> extends BaseEntity<M> {
	
	public static final String TABLE_NAME = "#(table.name.toLowerCase())"; //数据表名称
	
	public static final String TABLE_PK = "#(table.primaryKey.toLowerCase())"; //数据表主键
	
	public static final String TABLE_REMARK = "#(table.remarks)"; //数据表备注

	public String getTableName(){
		return TABLE_NAME;
	}

	public String getTableRemark(){
		return TABLE_REMARK;
	}
	
	public String getTablePK(){
		return TABLE_PK;
	}
	
   	#for(x : table.columnMetas)
   		
	/**
	 * Column ：#(x.name.toLowerCase())
	 * @return #(x.remarks)
	 */
   	#(x.toValidator())	
	public #(x.javaType) get#(firstCharToUpperCase(x.attrName))(){
   		return get("#(x.name.toLowerCase())");
   	}
	
	public void set#(firstCharToUpperCase(x.attrName))(#(x.javaType) #(x.attrName)){
   		set("#(x.name.toLowerCase())" , #(x.attrName));
   	}	
	#end	
}
